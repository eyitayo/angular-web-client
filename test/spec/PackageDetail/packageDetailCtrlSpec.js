'use strict';

describe('Package Detial Controller Test', function() {

    var controller, scope, packageDetailSvcMock;

    beforeEach(module('studiocdnWebApp'));

    beforeEach(inject(function($controller, $rootScope, $q) {
        scope = $rootScope.$new();
        controller = $controller;

        controller('PackageDetailCtrl', {
            $scope: scope,
        });
    }));

    it('should have toTitleCase function and change string to title case', inject(function() {
        expect(scope).to.have.property('toTitleCase');
        var actualVal = scope.toTitleCase('test');
        expect(actualVal).to.be.equal('Test');
    }));

    it('should have distribution property as object type', inject(function() {
        expect(scope).to.have.property('distribution');
    }));
});
