'use strict';

describe('Recipients control testing', function() {
    var controller, scope, userSvcMock, recipientsSvcMock;
    var data = {};
    var recipientHistory = [{
        "mailbox": "user@abc.com"
    }, {
        "mailbox": "user2@abc.com"
    }, {
        "mailbox": "test@abc.com"
    }, {
        "mailbox": "test2@abc.com"
    }, {
        "mailbox": "to@to.com"
    }, {
        "mailbox": "hello@hello.com"
    }];

    beforeEach(module('studiocdnWebApp'));

    beforeEach(module(function($provide) {
        $provide.value('recipient.email', 'bebert@m23l.com');
    }));

    beforeEach(inject(function($controller, $rootScope, _cacheWrapperSvc_, $q, _$httpBackend_) {
        scope = $rootScope.$new();
        controller = $controller;

        _$httpBackend_.whenGET('https://io.studiocdn.com/current-user').respond(200, '');

        var recipients = [{
            'id': '2RJR2A4P6T62EWX53CJ7VMPE8A',
            'mailbox': 'bebert@m23l.com'
        }, {
            'id': '38TTM51TESC7BAWEF872TCQFA9',
            'mailbox': 'toredola@m23l.com'
        }, {
            'id': '39BQ6HMZAQKD95CQGT9SCXJKQY',
            'mailbox': 'cnuesa@m23l.com'
        }];
        data = recipients;
        var result = {};

        recipientsSvcMock = {
            addRecipient: function() {

                return scope.recipient.email;
            },
            fetchRecipients: function() {
                result.data = data;
                var deferred = $q.defer();
                deferred.resolve(result);
                return deferred.promise;
            },
            getRecipients: function() {
                console.log("getRecipients")
                return [];
            },
            setRecipients: function(result) {
                this.recipients = result;
            },
            removeRecipient: function(recipient) {

            },
            getRecipientId: function(recipient) {
                return '2RJR2A4P6T62EWX53CJ7VMPE8A';
            },
            putRecipient: function(recipient) {
                return 200;
            },
            deleteRecipientById: function(id) {

            },
            addRecipientHistory: function(email) {
                var result = {
                    data: {
                        "_links": {
                            "self": {
                                "href": "https://io.studiocdn.com/users/38YDTPYKX4S77B0J9ZNGECZDVT/recipient-history"
                            },
                            "up": {
                                "href": "https://io.studiocdn.com/users/38YDTPYKX4S77B0J9ZNGECZDVT"
                            }
                        },
                        "elements": [{
                            "mailbox": email
                        }, {
                            "mailbox": "test@test.com"
                        }]
                    }
                };

                var deferred = $q.defer();
                deferred.resolve(result);
                return deferred.promise;
            }
        };

        userSvcMock = {
            recipientHistory: null,
            recipientHistoryCacheId: 'id',
            maxAge: 180000,
            user: {
                _links: {
                    recipientHistory: {
                        href: 'http://local'
                    }
                }
            },
            fetchUserRecipientHistoryDefault: function() {
                var result = {};
                result.data = {
                    "_links": {
                        "self": {
                            "href": "https://io.studiocdn.com/users/38YDTPYKX4S77B0J9ZNGECZDVT/recipient-history"
                        },
                        "up": {
                            "href": "https://io.studiocdn.com/users/38YDTPYKX4S77B0J9ZNGECZDVT"
                        }
                    },
                    "elements": recipientHistory
                };
                var deferred = $q.defer();
                deferred.resolve(result);
                return deferred.promise;
            },
            setUserRecipientHistory: function(result) {
                this.recipientHistory = result;
            },
            processResult: function(result) {
                this.user = {};
                this.user.mailbox = 'mailbox';

                this.user.data = result;
            }
        };

        controller('RecipientsCtrl', {
            $scope: scope,
            recipientsSvc: recipientsSvcMock,
        });

        scope.recipient.email = 'bebert@m23l.com';

    }));

    it('should pass the recipient email address to the service', function() {
        scope.$digest();
        expect(scope.recipient.email).to.equal('bebert@m23l.com');
        scope.addRecpEmail();
        expect(recipientsSvcMock.addRecipient()).to.have.been.called;
        expect(scope.recipientList).to.include('bebert@m23l.com');
    });
    it('should not add the same recipient twice', function() {
        scope.$digest();
        console.log(scope.recipientList)
        expect(scope.recipient.email).to.equal('bebert@m23l.com');
        scope.addRecpEmail();
        scope.recipient.email = 'toredola@m23l.com';
        scope.addRecpEmail();
        scope.recipient.email = 'bebert@m23l.com';
        scope.addRecpEmail();
        expect(scope.recipientList).to.have.length(2);
    });
    it('should not add the same recipient twice', function() {
        scope.$digest();
        console.log(scope.recipientList)
        expect(scope.recipient.email).to.equal('bebert@m23l.com');
        scope.addRecpEmail();
        scope.recipient.email = 'toredola@m23l.com';
        scope.addRecpEmail();
        scope.recipient.email = 'Ben<bebert@m23l.com>';
        scope.addRecpEmail();
        scope.recipient.email = 'BEBERT@m23l.com';
        scope.addRecpEmail();
        expect(scope.recipientList).to.have.length(2);
    });

    it('should validate a list with correct recipients', function() {
        var csvRecipients = ['bebert@m23l.com', 'rshah@m23l.com', 'TAYO <toredola@m23l.com>'];

        var isValid = scope.validateRecipients(csvRecipients);
        expect(isValid).to.equal(true);
    });

    it('should reject a list with duplicate addresses', function() {
        var csvRecipients = ['bebert@m23l.com', 'rshah@m23l.com', 'BEN<bebert@m23l.com>'];

        var isValid = scope.validateRecipients(csvRecipients);
        expect(isValid).to.equal(false);

    });

    it('should set the contents of the recipients block to a blank after consuming the recipient', function() {
        scope.$digest();
        scope.addRecpEmail();
        expect(scope.recipient.email).to.equal('');
    });

});
