'use strict';

describe('Draft Service', function() {
    var draftSvc;
    var host = 'https://io.studiocdn.com';
    var draftPath = '/distributions';

    beforeEach(module('studiocdnWebApp'));
    beforeEach(inject(function(_draftSvc_) {
        draftSvc = _draftSvc_;
    }));

    it('should have a Draft Service', inject(function() {
        expect(draftSvc).to.not.be.an('undefined');
    }));

    it('should create distribution url', inject(function() {
        expect(draftSvc).to.have.property('getDistUrl');

        var url = draftSvc.getDistUrl();
        expect(url).to.be.equal(host + draftPath);
    }));


    it('should have one parameter in fetch funtion ', inject(function() {
        expect(draftSvc.fetch).to.have.length(1);
    }));

    it('should process http return', inject(['$location', function($location) {
        expect(draftSvc).to.have.property('processResult');

        var data = {
            '_embedded': {
                'https://what.msiops.com/rel/status': {
                    '_links': {
                        'self': {
                            'href': 'https://io.studiocdn.com/distributions/3HJSJVYSMXX7KN22JZRVGXS16A/status'
                        },
                        'up': {
                            'href': 'https://io.studiocdn.com/distributions/3HJSJVYSMXX7KN22JZRVGXS16A'
                        }
                    },
                    'state': 'DRAFT'
                },
                'https://what.msiops.com/rel/metadata': {
                    '_links': {
                        'self': {
                            'href': 'https://io.studiocdn.com/distributions/3HJSJVYSMXX7KN22JZRVGXS16A/metadata'
                        },
                        'up': {
                            'href': 'https://io.studiocdn.com/distributions/3HJSJVYSMXX7KN22JZRVGXS16A'
                        }
                    }
                },
                'https://what.msiops.com/rel/options': {
                    'duration': 1,
                    'watermark': false,
                    '_links': {
                        'self': {
                            'href': 'https://io.studiocdn.com/distributions/3HJSJVYSMXX7KN22JZRVGXS16A/options'
                        },
                        'up': {
                            'href': 'https://io.studiocdn.com/distributions/3HJSJVYSMXX7KN22JZRVGXS16A'
                        }
                    },
                    'protectLinks': false,
                    'allowForward': false,
                    'allowDownload': false
                },
                'https://what.msiops.com/rel/notification': {
                    '_links': {
                        'self': {
                            'href': 'https://io.studiocdn.com/distributions/3HJSJVYSMXX7KN22JZRVGXS16A/notification'
                        },
                        'up': {
                            'href': 'https://io.studiocdn.com/distributions/3HJSJVYSMXX7KN22JZRVGXS16A'
                        }
                    }
                }
            },
            '_links': {
                'https://what.msiops.com/rel/recipients': {
                    'href': 'https://io.studiocdn.com/distributions/3HJSJVYSMXX7KN22JZRVGXS16A/recipients'
                },
                'self': {
                    'href': 'https://io.studiocdn.com/distributions/3HJSJVYSMXX7KN22JZRVGXS16A'
                },
                'https://what.msiops.com/rel/assets': {
                    'href': 'https://io.studiocdn.com/distributions/3HJSJVYSMXX7KN22JZRVGXS16A/assets'
                },
                'collection': {
                    'href': 'https://io.studiocdn.com/distributions'
                }
            }
        };

        var result = {
            data: data
        };

        result.headers = function(id) {
            return 'https://io.studiocdn.com/distributions/3HJSJVYSMXX7KN22JZRVGXS16A'
        };

        draftSvc.processResult(result);

        expect(draftSvc.resourceLocation).to.not.be.an('undefined');
        expect(draftSvc.draft).to.not.be.an('undefined');
        expect(draftSvc.draft.status).to.not.be.an('undefined');
        expect(draftSvc.draft.metadata).to.not.be.an('undefined');
        expect(draftSvc.draft.options).to.not.be.an('undefined');
        expect(draftSvc.draft.notification).to.not.be.an('undefined');
        expect(draftSvc.draft._links).to.not.be.an('undefined');

    }]));

});
