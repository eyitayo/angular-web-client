    'use strict';

    describe('Send view controller', function() {

        // load the controller's module
        beforeEach(module('studiocdnWebApp'));

        var SendCtrl, rootScope, scope;
        var pkgResult = {};
        var $window;

        // Initialize the controller and a mock scope
        beforeEach(inject(function($controller, $rootScope, $q, _cacheWrapperSvc_) {
            rootScope = $rootScope;
            scope = rootScope.$new();
            scope.email = 'test@test.com';
            scope.pswd = '123';
        }));

        it('should default send button as disabled', function() {
            expect(scope.isDisabled).to.be.true;
        });

        it('should enable send button', function() {
            scope.recReceived = true;
            scope.subjReceived = true;
            scope.linkIncluded = true;
            scope.assetReceived = true;
            scope.validatePage();
            expect(scope.isDisabled).to.be.false;
        });

        it('should disable send button', function() {
            scope.recReceived = false;
            scope.subjReceived = false;
            scope.linkIncluded = false;
            scope.assetReceived = false;
            scope.validatePage();
            expect(scope.isDisabled).to.be.true;
        });

        it('should update status to SCHEDULED', function() {
            scope.sendPkg();
            scope.$digest();
            expect(scope.state).to.equal('SCHEDULED');
            expect(cacheWrapperSvc.getCache('recipientHistoryCache')).to.be.an('undefined');
        });

    });
