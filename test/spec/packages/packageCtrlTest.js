'use strict';

describe('Package view controller', function () {
    var PackageCtrl, scope, packageSvcMock;

    // load the controller's module
    beforeEach(module('studiocdnWebApp'));

    // Initialize the controller 
    beforeEach(inject(function ($controller, $rootScope, $q) {
        scope = $rootScope.$new();

        packageSvcMock = {
            withSearchQuery: null,
            nextIndex: null,
            setAuthentication: function (auth) {},
            getPackages: function () {
                var deferred = $q.defer();
                deferred.reject();
                return deferred.promise;
            },
            resetPackagesNext: function (auth) {
                this.nextIndex = null;
            },
            getMorePackages: function () {
                var deferred = $q.defer();
                deferred.reject();
                return deferred.promise;
            }
        };

        PackageCtrl = $controller('PackageCtrl', {
            $scope: scope,
            packageSvc: packageSvcMock
        });
    }));

    it('should get set withQuery indicator to null', function () {
        packageSvcMock.withSearchQuery = "gaga";
        packageSvcMock.nextIndex = "next";

        scope.resetPackagesNext();

        expect(packageSvcMock.nextIndex).to.be.null;
    });
});
