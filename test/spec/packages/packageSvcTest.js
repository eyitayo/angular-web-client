'use strict';

describe('Package view service',
    function() {
        var packageSvc, $httpBackend;
        var input = {
            '_embedded': {
                'item': [{
                    '_embedded': {
                        'https://what.msiops.com/rel/status': {
                            '_links': {
                                'self': {
                                    'href': 'https://io.studiocdn.com/distributions/1CMT7NZRDN2Z65J10TPB9F1MRS/status'
                                },
                                'up': {
                                    'href': 'https://io.studiocdn.com/distributions/1CMT7NZRDN2Z65J10TPB9F1MRS'
                                }
                            },
                            'state': 'EXPIRED'
                        },
                        'https://what.msiops.com/rel/metadata': {
                            '_links': {
                                'self': {
                                    'href': 'https://io.studiocdn.com/distributions/1CMT7NZRDN2Z65J10TPB9F1MRS/metadata'
                                },
                                'up': {
                                    'href': 'https://io.studiocdn.com/distributions/1CMT7NZRDN2Z65J10TPB9F1MRS'
                                }
                            }
                        },
                        'https://what.msiops.com/rel/options': {
                            'duration': 1,
                            'watermark': false,
                            '_links': {
                                'self': {
                                    'href': 'https://io.studiocdn.com/distributions/1CMT7NZRDN2Z65J10TPB9F1MRS/options'
                                },
                                'up': {
                                    'href': 'https://io.studiocdn.com/distributions/1CMT7NZRDN2Z65J10TPB9F1MRS'
                                }
                            },
                            'protectLinks': false,
                            'allowForward': false,
                            'allowDownload': false
                        },
                        'https://what.msiops.com/rel/notification': {
                            '_links': {
                                'self': {
                                    'href': 'https://io.studiocdn.com/distributions/1CMT7NZRDN2Z65J10TPB9F1MRS/notification'
                                },
                                'up': {
                                    'href': 'https://io.studiocdn.com/distributions/1CMT7NZRDN2Z65J10TPB9F1MRS'
                                }
                            }
                        }
                    },
                    '_links': {
                        'https://what.msiops.com/rel/recipients': {
                            'href': 'https://io.studiocdn.com/distributions/1CMT7NZRDN2Z65J10TPB9F1MRS/recipients'
                        },
                        'self': {
                            'href': 'https://io.studiocdn.com/distributions/1CMT7NZRDN2Z65J10TPB9F1MRS'
                        },
                        'https://what.msiops.com/rel/assets': {
                            'href': 'https://io.studiocdn.com/distributions/1CMT7NZRDN2Z65J10TPB9F1MRS/assets'
                        },
                        'collection': {
                            'href': 'https://io.studiocdn.com/distributions'
                        }
                    }
                }, {
                    '_embedded': {
                        'https://what.msiops.com/rel/status': {
                            '_links': {
                                'self': {
                                    'href': 'https://io.studiocdn.com/distributions/1W4TDBMKYJAH8V97WG797H6DMY/status'
                                },
                                'up': {
                                    'href': 'https://io.studiocdn.com/distributions/1W4TDBMKYJAH8V97WG797H6DMY'
                                }
                            },
                            'state': 'ACTIVE'
                        },
                        'https://what.msiops.com/rel/metadata': {
                            '_links': {
                                'self': {
                                    'href': 'https://io.studiocdn.com/distributions/1W4TDBMKYJAH8V97WG797H6DMY/metadata'
                                },
                                'up': {
                                    'href': 'https://io.studiocdn.com/distributions/1W4TDBMKYJAH8V97WG797H6DMY'
                                }
                            }
                        },
                        'https://what.msiops.com/rel/options': {
                            'duration': 1,
                            'watermark': false,
                            '_links': {
                                'self': {
                                    'href': 'https://io.studiocdn.com/distributions/1W4TDBMKYJAH8V97WG797H6DMY/options'
                                },
                                'up': {
                                    'href': 'https://io.studiocdn.com/distributions/1W4TDBMKYJAH8V97WG797H6DMY'
                                }
                            },
                            'protectLinks': false,
                            'allowForward': false,
                            'allowDownload': false
                        },
                        'https://what.msiops.com/rel/notification': {
                            '_links': {
                                'self': {
                                    'href': 'https://io.studiocdn.com/distributions/1W4TDBMKYJAH8V97WG797H6DMY/notification'
                                },
                                'up': {
                                    'href': 'https://io.studiocdn.com/distributions/1W4TDBMKYJAH8V97WG797H6DMY'
                                }
                            },
                            'body': '<br><a href=\'{$link}\'>Click here to view</a>'
                        }
                    },
                    '_links': {
                        'https://what.msiops.com/rel/recipients': {
                            'href': 'https://io.studiocdn.com/distributions/1W4TDBMKYJAH8V97WG797H6DMY/recipients'
                        },
                        'self': {
                            'href': 'https://io.studiocdn.com/distributions/1W4TDBMKYJAH8V97WG797H6DMY'
                        },
                        'https://what.msiops.com/rel/assets': {
                            'href': 'https://io.studiocdn.com/distributions/1W4TDBMKYJAH8V97WG797H6DMY/assets'
                        },
                        'collection': {
                            'href': 'https://io.studiocdn.com/distributions'
                        }
                    }
                }, {
                    '_embedded': {
                        'https://what.msiops.com/rel/status': {
                            '_links': {
                                'self': {
                                    'href': 'https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T/status'
                                },
                                'up': {
                                    'href': 'https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T'
                                }
                            },
                            'state': 'Draft'
                        },
                        'https://what.msiops.com/rel/metadata': {
                            '_links': {
                                'self': {
                                    'href': 'https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T/metadata'
                                },
                                'up': {
                                    'href': 'https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T'
                                }
                            },
                            'albumTitle': 'number 1',
                            'artistName': 'Nobody'
                        },
                        'https://what.msiops.com/rel/options': {
                            '_links': {
                                'self': {
                                    'href': 'https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T/options'
                                },
                                'up': {
                                    'href': 'https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T'
                                }
                            }
                        },
                        'https://what.msiops.com/rel/notification': {
                            '_links': {
                                'self': {
                                    'href': 'https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T/notification'
                                },
                                'up': {
                                    'href': 'https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T'
                                }
                            },
                            'subject': 'my first distribution',
                            'body': '<br><a href=\'{$link}\'>Click here to view</a>'
                        }
                    },
                    '_links': {
                        'https://what.msiops.com/rel/recipients': {
                            'href': 'https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T/recipients'
                        },
                        'self': {
                            'href': 'https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T'
                        },
                        'https://what.msiops.com/rel/assets': {
                            'href': 'https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T/assets'
                        },
                        'collection': {
                            'href': 'https://io.studiocdn.com/distributions'
                        }
                    }
                }]
            },
            '_links': {
                'next': {
                    'href': 'https://io.studiocdn.com/distributions?start=9223370608747149827'
                },
                'self': {
                    'href': 'https://io.studiocdn.com/distributions'
                }
            }
        };

        beforeEach(module('studiocdnWebApp'));

        beforeEach(inject(function(_packageSvc_, _$httpBackend_) {
            $httpBackend = _$httpBackend_;
            packageSvc = _packageSvc_;

            $httpBackend.whenGET(/[main|packages].html/).respond(200, '');
            $httpBackend.whenGET('modules/packages/partial-all.html').respond(200, '');
            $httpBackend.whenGET('https://io.studiocdn.com/current-user').respond(200, '');
            $httpBackend.whenPOST('https://spark.studiocdn.com/tokens').respond(200, '');
            $httpBackend.whenPOST('https://io.studiocdn.com/users').respond(200, '');
        }));

        it('should get a url to get all packages', function() {

            var anURL = packageSvc.getPackagesUrl();

            expect(anURL).to.exist;
            expect(anURL).to.be.equal('https://io.studiocdn.com/distributions');
        });


        it('should be able to replace the title of a package', function() {

            var draft = {
                'title': 'StudioCDN Delivery',
                'notificationURL': 'url',
                'artistName': 'artist',
                'pkgStatus': 'Draft',
                'self': 'temp'
            };
            var draft2 = {
                'title': 'StudioCDN Delivery',
                'notificationURL': 'url',
                'artistName': 'artist',
                'pkgStatus': 'Draft',
                'self': 'temp1'
            };
            var draft3 = {
                'title': 'StudioCDN Delivery',
                'notificationURL': 'url',
                'artistName': 'artist',
                'pkgStatus': 'Draft',
                'self': 'temp2'
            };

            packageSvc.draftList.push(draft);
            packageSvc.draftList.push(draft2);
            packageSvc.draftList.push(draft3);

            packageSvc.updateTitle('temp2', 'new title');
            expect(packageSvc.draftList[2].title).to.equal('new title');

        });

        it('should be able to convert a list of packages into the correct format', function() {

            packageSvc.setDraftList(input);

            expect(packageSvc.draftList).to.have.length(3);
            expect(packageSvc.draftList[2].artistName).to.equal('Nobody');
            expect(packageSvc.draftList[2].pkgStatus).to.equal('draft');
            expect(packageSvc.draftList[2].self).to.equal('https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T');
            expect(packageSvc.draftList[2].notificationURL).to.equal('https://io.studiocdn.com/distributions/3ERK0CJPK381PMRJ4QERB71R2T/notification');

        });
        it('should be able to append data to the draftlist',
            function() {

                packageSvc.setDraftList(input);
                packageSvc.appendToDraftList(input);

                expect(packageSvc.draftList).to.have.length(6);
                expect(packageSvc.draftList[2].artistName).to.equal('Nobody');
                expect(packageSvc.draftList[5].artistName).to.equal('Nobody');

            });

        it('should be able to filter & convert a list of packages into the correct format', function() {
            packageSvc.setDraftList(input);
            expect(packageSvc.draftList).to.have.length(3);
            packageSvc.setFilter = [];
            packageSvc.addFilter('active');
            packageSvc.setDraftList(input);
            expect(packageSvc.draftList).to.have.length(1);

            packageSvc.setFilter = [];
            packageSvc.addFilter('expired');
            packageSvc.setDraftList(input);
            expect(packageSvc.draftList).to.have.length(1);

            packageSvc.setFilter = [];
            packageSvc.addFilter('draft');
            packageSvc.setDraftList(input);
            expect(packageSvc.draftList).to.have.length(1);

            packageSvc.setFilter = [];
            packageSvc.addFilter('whatever');
            packageSvc.setDraftList(input);
            expect(packageSvc.draftList).to.have.length(0);
        });
    });
