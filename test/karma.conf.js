// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2014-12-01 using
// generator-karma 0.8.3

module.exports = function(config) {
    'use strict';

    config.set({
        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // base path, that will be used to resolve files and exclude
        basePath: '../',

        // testing framework to use (jasmine/mocha/qunit/...)
        frameworks: ['mocha', 'chai'],

        // list of files / patterns to load in the browser
        files: [
            'bower_components/jquery/dist/jquery.js',
            'bower_components/angular/angular.js',
            'bower_components/angular-mocks/angular-mocks.js',
            'bower_components/angular-animate/angular-animate.js',
            'bower_components/angular-cookies/angular-cookies.js',
            'bower_components/angular-resource/angular-resource.js',
            'bower_components/angular-route/angular-route.js',
            'bower_components/angular-sanitize/angular-sanitize.js',
            'bower_components/angular-touch/angular-touch.js',
            'bower_components/angular-strap/dist/angular-strap.js',
            'bower_components/angular-strap/dist/angular-strap.tpl.js',
            'bower_components/angular-cache/dist/angular-cache.min.js',
            'bower_components/angular-jwt/dist/angular-jwt.min.js',
            'bower_components/ckeditor/ckeditor.js',
            'bower_components/ng-file-upload/angular-file-upload.js',
            'bower_components/angular-ui-router/release/angular-ui-router.js',
            'node_modules/node-forge/js/sha256.js',
            'node_modules/node-forge/js/md5.js',
            'node_modules/node-forge/js/hmac.js',
            'node_modules/node-forge/js/sha1.js',
            'node_modules/node-forge/js/util.js',
            'app/modules/inbox/inbox.js',
            'app/modules/packages/package.js',
            'app/**/*.js',
            // 'test/mock/**/*.js',
            'test/spec/**/*.js'
        ],

        // list of files / patterns to exclude
        exclude: [

        ],

        // web server port
        // port: 8080,
        port: 8181,

        browsers: [
            'Chrome_small'
        ],

        reporters: ['mocha'],

        customLaunchers: {
            Chrome_small: {
                base: 'Chrome',
                flags: ['--window-size=600,200']
            }
        },

        // Which plugins to enable
        plugins: [
            'karma-mocha',
            'karma-chai',
            'karma-chrome-launcher',
            'karma-mocha-reporter'
        ],

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false,

        colors: true,

        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,

        // Uncomment the following lines if you are using grunt's server to run the tests
        proxies: {
            '/workers': 'http://localhost:8181/base/app/modules/commons/workers',
            '/node_modules/node-forge/js': 'http://localhost:8181/base/node_modules/node-forge/js',
        },
        // URL root prevent conflicts with the site root
        // urlRoot: '_karma_'
    });
};
