'use strict';

var studiocdnWebApp = angular.module('studiocdnWebApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'cacheWrapperApp',
        'mgcrea.ngStrap',
        'ui.router',
        'mainPackageApp',
        'sendPackageApp',
        'recipientsApp',
        'packageApp',
        'packageDetailApp',
        'jwtAuthenticationApp'
    ]).constant('appSettings', {
        restApiHost: 'https://io.studiocdn.com',
        draftResource: '/#/send',
        distDetailResource: '/#/package/detail/list',
        uploadResource: 'https://io.studiocdn.com/quarantine',
        postFixCurrentUserEndPoint: '/current-user',
        postFixUsersEndPoint: '/users',
        distributionEndpoint: '/distributions',
        postFixUserPreferences: '/preferences',
        postFixUserPermissions: '/permissions',
        postfixUserEndpoint: '/users',
        postfixCoverArtEndPoint: '/cover-art',
        postFixSearchDist: '/searchdistributions',
        setUrlParams: function($location, path, id, state) {
            for (var i in $location.search()) {
                $location.search(i, null);
            }
            $location.search('dist', id);
            $location.search('state', state);
            if (path)
                $location.path(path);
        }
    })
    .config(function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider
                .otherwise('/packages/all');
            $stateProvider
                .state('main', {
                    url: '',
                    abstract: true,
                    templateUrl: 'modules/mainPackage/main.html',
                    controller: 'MainCtrl'
                })
                .state('main.packages', {
                    url: '/packages',
                    templateUrl: 'modules/packages/packages.html',
                    controller: 'PackageCtrl',
                })
                .state('main.packages.All', {
                    url: '/all',
                    templateUrl: 'modules/packages/partial-all.html',
                    controller: 'PackageCtrl',
                })
                .state('main.packages.Active', {
                    url: '/active',
                    templateUrl: 'modules/packages/partial-active.html',
                    controller: 'PackageCtrl',
                })
                .state('main.packages.Expired', {
                    url: '/expired',
                    templateUrl: 'modules/packages/partial-expired.html',
                    controller: 'PackageCtrl',
                })
                .state('main.packages.Drafts', {
                    url: '/drafts',
                    templateUrl: 'modules/packages/partial-drafts.html',
                    controller: 'PackageCtrl',
                })
                .state('main.detail', {
                    url: '/package/detail',
                    templateUrl: 'modules/packageDetail/details.html',
                    controller: 'PackageDetailCtrl'
                })
                .state('main.detail.list', {
                        url: '/list',
                        templateUrl: 'modules/packageDetail/partial-detail-all.html',
                    }
                })
        .state('main.detail.activity', {
            url: '/activity?dist&recip',
            templateUrl: 'modules/packageDetail/partial-detail-activity.html',
        })
        .state('main.send', {
            url: '/send',
            templateUrl: 'modules/sendPackage/send.html',
            controller: 'SendCtrl',
        })
        .state('login', {
            url: '/login',
            templateUrl: 'modules/commons/login.html',
            controller: 'MainCtrl'
        })
        .state('recover', {
            url: '/recover',
            templateUrl: 'modules/commons/recoverPwd.html',
            controller: 'MainCtrl'
        })
        .state('recoverinfo', {
            url: '/recoverinfo',
            templateUrl: 'modules/commons/recoverPwdInfo.html',
            controller: 'MainCtrl'
        });
    });

studiocdnWebApp.run(['$rootScope', '$state', '$urlRouter', '$injector', 'routerSvc', function($rootScope, $state, $urlRouter, $injector, routerSvc) {
    $rootScope.$on('$stateChangeError', function(event, next) {
        $state.go($rootScope.redirectTo || 'login');
    });
}]);
