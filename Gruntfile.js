// Generated on 2014-12-01 using generator-angular 0.10.0
'use strict';

module.exports = function(grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Configurable paths for the application
    var appConfig = {
        app: require('./bower.json').appPath || 'app',
        dist: 'dist'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        yeoman: appConfig,

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= yeoman.app %>/{,*/}/{,*/}*.html',
                    '.tmp/styles/{,*/}*.css',
                    '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            },

            compass: {
                files: ['<%= yeoman.app %>/styles/**/*.{scss,sass}'],
                tasks: ['compass:server']
            },
            beautify: {
                files: ['<%= yeoman.app %>/**/*.js', 'test/spec/**/*.js', '<%= yeoman.app %>*.js'],
                tasks: ['jsbeautifier:beautify'],
                options: {
                    livereload: '<%= connect.options.livereload %>'
                }
            },
            clear: {
                //clear terminal on any watch task. beauty.
                files: ['<%= yeoman.app %>/**/*.js', 'test/spec/**/*.js'],
                tasks: ['clear']
            },
            bower: {
                files: ['bower.json'],
                tasks: ['wiredep']
            },
            js: {
                files: ['<%= yeoman.app %>/modules/**/*.js', '<%= yeoman.app %>/app.js'],
                tasks: ['newer:jshint:all'],
                options: {
                    livereload: '<%= connect.options.livereload %>'
                }
            },
            jsTest: {
                files: ['test/spec/**/*.js', '<%= yeoman.app %>/**/*.js'],
                tasks: ['karma'],
                options: {
                    livereload: '<%= connect.options.livereload %>'
                }
            },
            gruntfile: {
                files: ['Gruntfile.js']
            }
        },

        // The actual grunt server settings
        connect: {
            options: {
                port: 9000,
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: 'localhost',
                livereload: 35729
            },
            livereload: {
                options: {
                    open: true,
                    middleware: function(connect) {
                        return [
                            connect.static('.tmp'),
                            connect().use(
                                '/bower_components',
                                connect.static('./bower_components')
                            ),
                            connect().use(
                                '/node_modules',
                                connect.static('./node_modules')
                            ),
                            // load modules needed for workers
                            connect().use(
                                '/modules/commons/node_modules/node-forge/js',
                                connect.static('./node_modules/node-forge/js')
                            ),
                            connect().use(
                                '/workers',
                                connect.static('./' + appConfig.app + '/modules/commons/workers')
                            ),
                            connect.static(appConfig.app)
                        ];
                    }
                }
            },
            test: {
                options: {
                    port: 9001,
                    middleware: function(connect) {
                        return [
                            connect.static('.tmp'),
                            connect.static('test'),
                            connect().use(
                                '/bower_components',
                                connect.static('./bower_components')
                            ),
                            connect().use(
                                '/node_modules',
                                connect.static('./node_modules')
                            ),
                            connect.static(appConfig.app)
                        ];
                    }
                }
            },
            dist: {
                options: {
                    open: true,
                    base: '<%= yeoman.dist %>'
                }
            }
        },

        jsbeautifier: {
            beautify: {
                src: ['<%= yeoman.app %>/**/*.js', 'test/spec/**/*.js', '<%= yeoman.app %>*.js'],
                options: {
                    config: '.jsbeautifyrc'
                }
            }
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: {
                src: [
                    'Gruntfile.js',
                    '<%= yeoman.app %>/modules/**/*.js',
                    '<%= yeoman.app %>/app.js',
                    '!<%= yeoman.app %>/modules/commons/ckeditor/ck_custom_config/**/*.js',
                    '!<%= yeoman.app %>/styles/ckeditor_skin/bootstrapck/*'

                ]
            },
            test: {
                options: {
                    jshintrc: 'test/.jshintrc'
                },
                src: ['test/spec/**/*.js']
            }
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= yeoman.dist %>/{,*/}*',
                        '!<%= yeoman.dist %>/.git{,*/}*'
                    ]
                }]
            },
            server: '.tmp'
        },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/styles/',
                    src: '{,*/}*.css',
                    dest: '.tmp/styles/'
                }]
            }
        },

        // Automatically inject Bower components into the app
        wiredep: {
            app: {
                src: ['<%= yeoman.app %>/index.html'],
                ignorePath: /\.\.\//,
                exclude: ['bower_components/bootstrap-sass-official/assets/javascripts'],
                devDependencies: true,
                dependencies: true
            },

            sass: {
                src: ['<%= yeoman.app %>/styles/**/*.{scss,sass}'],
                ignorePath: /(\.\.\/){1,2}bower_components\//
            }
        },

        // Compiles Sass to CSS and generates necessary files if requested
        compass: {
            options: {
                sassDir: '<%= yeoman.app %>/styles',
                cssDir: '.tmp/styles',
                generatedImagesDir: '.tmp/images/generated',
                imagesDir: '<%= yeoman.app %>/images',
                javascriptsDir: '<%= yeoman.app %>',
                fontsDir: '<%= yeoman.app %>/styles/fonts',
                importPath: './bower_components',
                httpImagesPath: '/images',
                httpGeneratedImagesPath: '/images/generated',
                httpFontsPath: '/styles/fonts',
                relativeAssets: true,
                assetCacheBuster: false,
                raw: 'Sass::Script::Number.precision = 10\n'
            },
            dist: {
                options: {
                    generatedImagesDir: '<%= yeoman.dist %>/images/generated'
                }
            },
            server: {
                options: {
                    debugInfo: true
                }
            }
        },

        // Renames files for browser caching purposes
        filerev: {
            dist: {
                src: [
                    '<%= yeoman.dist %>/dist.js',
                    '<%= yeoman.dist %>/styles/{,*/}*.css',
                    '<%= yeoman.dist %>/bower_components/angular-motion/**/*.css',
                    '<%= yeoman.dist %>/bower_components/bootstrap/**/*.css',
                    '<%= yeoman.dist %>/styles/fonts/*',
                    '<%= yeoman.dist %>/images/*',
                    '<%= yeoman.dist %>/modules/**/*.html',
                    '<%= yeoman.dist %>/node_modules/**/*.js',
                    '<%= yeoman.dist %>/workers/**/*.js',
                    '<%= yeoman.dist %>/bower_components/es5-shim/**/*.js',
                    '<%= yeoman.dist %>/bower_components/json3/**/*.js',
                    '<%= yeoman.dist %>/bower_components/angular-mocks/**/*.js',
                    '<%= yeoman.dist %>/bower_components/angular-scenario/**/*.js',
                    '<%= yeoman.dist %>/bower_components/ng-file-upload/**/*.js',
                    '<%= yeoman.dist %>/bower_components/ng-file-upload-shim/**/*.js',
                    '<%= yeoman.dist %>/bower_components/bootstrap/**/*.js',
                    '<%= yeoman.dist %>/bower_components/angular-strap/**/*.js',
                    '<%= yeoman.dist %>/bower_components/angular-cache/**/*.js',
                    '<%= yeoman.dist %>/bower_components/angular-ui-router/**/*.js',
                    '<%= yeoman.dist %>/bower_components/angular-jwt/**/*.js'
                ]
            }
        },

        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            html: '<%= yeoman.app %>/index.html',
            options: {
                dest: '<%= yeoman.dist %>',
                flow: {
                    html: {
                        steps: {
                            js: ['concat', 'uglifyjs'],
                            css: ['cssmin']
                        },
                        post: {}
                    }
                }
            }
        },

        // Performs rewrites based on filerev and the useminPrepare configuration
        usemin: {
            html: ['<%= yeoman.dist %>/**/*.html'],
            index: ['<%= yeoman.dist %>/index.html'],
            modules: ['<%= yeoman.dist %>/modules/**/*.html'],
            node_modules: ['<%= yeoman.dist %>/workers/**/*.js'],
            css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
            js: ['<%= yeoman.dist %>/**/*.js'],
            options: {
                assetsDirs: ['<%= yeoman.dist %>', '<%= yeoman.dist %>/images'],
                patterns: {
                    js: [
                        [/(modules\/.*?\.(?:html))/gm, 'Update the JS to reference our revved html'],
                        [/(workers\/.*?\.(?:js))/gm, 'Update the JS to reference our revved js'],
                        [/(images\/.*?\.(?:gif))/gm, 'Update the JS to reference our revved gifs'],
                    ],
                    index: [
                        [/(modules\/.*?\.(?:html))/gm, 'Update index to reference our revved html'],
                        [/(bower_components\/.*?\.(?:js))/gm, 'Update index to reference our revved js']
                    ],
                    modules: [
                        [/(modules\/.*?\.(?:html))/gm, 'Update modules to reference our revved html']
                    ],
                    node_modules: [
                        [/(node_modules\/.*?\.(?:js))/gm, 'Update workers to reference our revved js']
                    ]
                },
            }
        },

        // The following *-min tasks will produce minified files in the dist folder
        // By default, your `index.html`'s <!-- Usemin block --> will take care of
        // minification. These next options are pre-configured if you do not wish
        // to use the Usemin blocks.
        cssmin: {
            dist: {
                files: {
                    '<%= yeoman.dist %>/styles/main.css': [
                        '.tmp/styles/{,*/}*.css'
                    ]
                }
            }
        },
        uglify: {
            dist: {
                files: {
                    'dist/<%= yeoman.dist %>.js': [
                        'dist/<%= yeoman.dist %>.js'
                    ]
                }
            }
        },
        concat: {
            options: {
                // define a string to put between each file in the concatenated output
                separator: ';'
            },
            dist: {
                // the files to concatenate
                src: ['<%= yeoman.app %>/*.js', '<%= yeoman.app %>/{,*/}/{,*/}*.js'],
                // the location of the resulting JS file
                dest: 'dist/<%= yeoman.dist %>.js'
            }
        },

        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/images',
                    src: '{,*/}*.{png,jpg,jpeg,gif}',
                    dest: '<%= yeoman.dist %>/images'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/images',
                    src: '{,*/}*.svg',
                    dest: '<%= yeoman.dist %>/images'
                }]
            }
        },

        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    removeOptionalTags: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.dist %>',
                    src: ['*.html', '{,*/}*/{,*/}*/{,*/}*.html'],
                    dest: '<%= yeoman.dist %>'
                }]
            }
        },

        // ng-annotate tries to make the code safe for minification automatically
        // by using the Angular long form for dependency injection.
        ngAnnotate: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/concat',
                    src: ['*.js', '!oldieshim.js'],
                    dest: '.tmp/concat'
                }]
            }
        },

        // Replace Google CDN references
        /* cdnify: {
             dist: {
                 html: ['<%= yeoman.dist %>/*.html']
             }
         },*/

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: '<%= yeoman.dist %>',
                    src: [
                        '*.{ico,png,txt}',
                        '*.html',
                        '<%= yeoman.dist %>.js',
                        '{,*/}*/{,*/}*/{,*/}*.html',
                        'images/{,*/}*.{webp}',
                        'fonts/{,*/}*.*'
                    ]
                }, {
                    expand: true,
                    cwd: '<%= yeoman.app %>/modules/commons/workers',
                    src: ['*.js'],
                    dest: '<%= yeoman.dist %>/workers'
                }, {
                    expand: true,
                    cwd: '.tmp/images',
                    dest: '<%= yeoman.dist %>/images',
                    src: ['generated/*']
                }, {
                    expand: true,
                    cwd: '.',
                    src: ['bower_components/jquery/dist/jquery.js',
                        'bower_components/angular/angular.js',
                        'bower_components/angular-animate/angular-animate.js',
                        'bower_components/angular-cookies/angular-cookies.js',
                        'bower_components/angular-resource/angular-resource.js',
                        'bower_components/angular-route/angular-route.js',
                        'bower_components/angular-sanitize/angular-sanitize.js',
                        'bower_components/angular-touch/angular-touch.js',
                        'bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*',
                        'bower_components/angular-scenario/angular-scenario.js',
                        'bower_components/es5-shim/es5-shim.js',
                        'bower_components/json3/lib/json3.js',
                        'bower_components/angular-mocks/angular-mocks.js',
                        'bower_components/angular-motion/dist/angular-motion.css',
                        'bower_components/bootstrap/dist/js/bootstrap.js',
                        'bower_components/angular-strap/dist/angular-strap.js',
                        'bower_components/angular-strap/dist/angular-strap.tpl.js',
                        'bower_components/bootstrap/dist/css/bootstrap.css',
                        'bower_components/ckeditor/skins/moono/editor.css',
                        'bower_components/bootstrap/dist/css/bootstrap.css.map',
                        'bower_components/ckeditor/**/*',
                        'bower_components/ng-file-upload/angular-file-upload.js',
                        'bower_components/ng-file-upload-shim/angular-file-upload-shim.js',
                        'bower_components/angular-cache/dist/angular-cache.js',
                        'bower_components/angular-cache/dist/angular-cache.min.js',
                        'bower_components/angular-cache/dist/angular-cache.min.map',
                        'bower_components/angular-ui-router/release/angular-ui-router.js',
                        'bower_components/angular-jwt/dist/angular-jwt.js'
                    ],
                    dest: '<%= yeoman.dist %>'
                }, {
                    expand: true,
                    cwd: '.',
                    src: ['node_modules/node-forge/js/sha256.js',
                        'node_modules/node-forge/js/md5.js',
                        'node_modules/node-forge/js/hmac.js',
                        'node_modules/node-forge/js/sha1.js',
                        'node_modules/node-forge/js/util.js'
                    ],
                    dest: '<%= yeoman.dist %>'
                }, {
                    expand: true,
                    cwd: '<%= yeoman.app %>',
                    src: ['modules/commons/ckeditor/ck_custom_config/*.js',
                        'modules/commons/ckeditor/ck_custom_config/ckeditor_skin/**/*',
                        'modules/recipients/recipients.json'
                    ],

                    dest: '<%= yeoman.dist %>'
                }]
            },
            styles: {
                expand: true,
                cwd: '<%= yeoman.app %>/styles',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            }
        },

        // Run some tasks in parallel to speed up the build process
        concurrent: {
            server: [
                'compass:server'
            ],
            test: [
                'compass'
            ],
            dist: [
                'compass:dist',
                'imagemin',
                'svgmin'
            ]
        },

        // Test settings
        karma: {
            unit: {
                configFile: 'test/karma.conf.js',
                singleRun: true
            }
        }
    });

    grunt.registerTask('serve', 'Compile then start a connect web server', function(target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'connect:dist:keepalive']);
        }

        grunt.task.run([
            'clean:server',
            'wiredep',
            'concurrent:server',
            'connect:livereload',
            'watch',
            'autoprefixer'
        ]);
    });

    grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function(target) {
        grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
        grunt.task.run(['serve:' + target]);
    });

    grunt.registerTask('test', [
        'clean:server',
        'concurrent:test',
        'autoprefixer',
        'connect:test',
        'jsbeautifier',
        'karma'
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'wiredep',
        'useminPrepare',
        'concurrent:dist',
        'autoprefixer',
        'concat',
        'ngAnnotate',
        'copy:dist',
        /*'cdnify',*/
        'cssmin',
        'uglify',
        'filerev',
        'usemin',
        'htmlmin'
    ]);

    grunt.registerTask('default', [
        'newer:jshint',
        'test',
        'build'
    ]);
};
