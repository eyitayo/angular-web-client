'use strict';

angular.module('sendPackageApp', ['authModule', 'packageOptionsApp', 'recipientsApp', 'notificationApp', 'metadataApp', 'fileControls', 'ckEditor', 'urlService'])
    .config(function ($tooltipProvider) {
        angular.extend($tooltipProvider.defaults, {
            html: true,
            placement: 'bottom'
        });
    })

.config(function ($timepickerProvider) {
    angular.extend($timepickerProvider.defaults, {
        length: 7
    });
});
