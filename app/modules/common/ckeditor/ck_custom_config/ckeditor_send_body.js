/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
/* jshint ignore:start */
'use strict';

CKEDITOR.editorConfig = function (config) {

    config.toolbar = [{
            name: 'basicstyles',
            groups: ['basicstyles', 'cleanup'],
            items: ['Bold', 'Italic', 'Underline']
        }, {
            name: 'paragraph',
            groups: ['list', 'align', 'bidi'],
            items: ['NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
        }, {
            name: 'styles',
            items: ['Font', 'FontSize']
        }, {
            name: 'colors',
            items: ['TextColor', 'BGColor']
        }, {
            name: 'links',
            items: ['Link', 'Unlink']
        }, {
            name: 'insert',
            items: ['Image', 'HorizontalRule']
        }, {
            name: 'other',
            items: ['RemoveFormat']
        },

    ];

    config.forcePasteAsPlainText = true;
    config.scayt_autoStartup = true;
    config.disableNativeSpellChecker = false;
    config.skin = 'bootstrapck,/modules/commons/ckeditor/ck_custom_config/ckeditor_skin/bootstrapck/';
    config.resize_enabled = false;
    config.removeButtons = 'Subscript,Superscript';
    config.enterMode = CKEDITOR.ENTER_BR;
    config.removePlugins = 'elementspath,liststyle,tabletools,scayt,menubutton,contextmenu, language, magicline';

};

/* jshint ignore:end */
